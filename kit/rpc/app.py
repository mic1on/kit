# -*- coding: utf-8 -*-
import time

from .job import JobMixin
from sanic import Sanic, json

from .state import State


class ServerMixin:

    def _heartbeat(self, request):
        return json({"hello": "job center"})

    def creat_app(self):
        self.server_app = Sanic("rpc_server")

        self.server_app.add_route(self._heartbeat, "/heartbeat", methods=["GET"])  # noqa
        return self.server_app

    def run(self, *args, **kwargs):
        self.server_app.run(*args, **kwargs)


class Rpc(JobMixin, ServerMixin):

    def __init__(self):
        self.app = self.creat_app()
        self.state = State()

    def run_forever(self):
        while True:
            time.sleep(1)


rpc = Rpc()
