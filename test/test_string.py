# -*- coding: utf-8 -*-
from kit.string_ import get_section


def test_get_section():
    assert get_section('abc', 'a', 'c') == 'b'
    assert get_section('abc', 'a') == 'bc'
    assert get_section('abc', end_str='c') == 'ab'
    assert get_section('abc') == 'abc'
