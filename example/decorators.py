# -*- coding: utf-8 -*-
from concurrent.futures import ThreadPoolExecutor, as_completed
import time

from kit.decorator import singleton


@singleton
class MyClass:
    pass


def test_singleton(x):
    print(MyClass() is MyClass())


def main():
    with ThreadPoolExecutor(max_workers=15) as t:
        obj_list = range(20)
        _iter = t.map(test_singleton, obj_list)

        for _ in _iter:
            pass
        print('ok')


if __name__ == '__main__':
    main()
