# -*- coding: utf-8 -*-
from kit.tool.timer import Timer, TimerManager

tm = TimerManager()


def _compare_and_trigger_listener():
    print("compare and trigger listener111")


def _compare_and_trigger_listener2():
    print("compare and trigger listener222")


timer_name = 'service-subscribe-timer'
subscribe_timer = Timer(name=timer_name,
                        interval=7,
                        fn=_compare_and_trigger_listener)
timer_name = 'service-subscribe-timer2'
subscribe_timer2 = Timer(name=timer_name,
                         interval=3,
                         fn=_compare_and_trigger_listener2)
# subscribe_timer.scheduler()
tm.add_timer(subscribe_timer)
tm.add_timer(subscribe_timer2)
tm.execute()
